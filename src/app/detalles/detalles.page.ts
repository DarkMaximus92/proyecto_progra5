import { Component, OnInit } from '@angular/core';
import { trago } from './detalles.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from './service.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-detalles',
  templateUrl: './detalles.page.html',
  styleUrls: ['./detalles.page.scss'],
})
export class DetallesPage implements OnInit {
  licorActual: trago;
  detalleID: number;

  constructor(
    private route: ActivatedRoute,
    public servicio: ServiceService,
    private router: Router,
    private alert: AlertController
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      if(!paramMap.has('detalleID')){
        return;
      }
      this.detalleID = parseInt(paramMap.get('detalleID'));
      this.licorActual = this.servicio.getBebida(this.detalleID);
    });    
  }

  borrarLicor(){
    this.alert.create({
      header: 'Borrar',
      message: 'Esta seguro que quiere borrar este licor?',
      buttons:[
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'Borrar',
          handler: () => {
            this.servicio.borrarLicor(this.detalleID);
            this.router.navigate(['/licor', this.licorActual.tipo]);
          }
        }
      ]
    })
    .then(alertEl => {
      alertEl.present();
    });
  }

}
