export interface trago{
    id: number;
    tipo: string;
    nombre: string;
    descripcion: string;
    imagen: string;
}