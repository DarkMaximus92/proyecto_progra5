import { Injectable } from '@angular/core';
import { trago } from './detalles.model';
import { NgForOf } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  private bebidas: trago[] = [
    {
      id: 1,
      tipo: "whiskey",
      nombre: "WILD TURKEY RYE",
      descripcion: "Wild Turkey Straight Rye Whiskey is an 81 proof (40.5% alcohol) ode to America’s first distilled spirit: rye whiskey.",
      imagen: "https://img.thewhiskyexchange.com/900/brbon_wil40.jpg",
    },
    {
      id: 2,
      tipo: "whiskey",
      nombre: "MACALLAN 18",
      descripcion: "Macallan's 18 Year Old triple-cask-matured single malt. Aged in a combination of sherry-seasoned European and American oak casks.",
      imagen: "https://img.thewhiskyexchange.com/900/macob.18yov7.jpg",
    },
    {
      id: 3,
      tipo: "whiskey",
      nombre: "JOHNNIE WALKER GREEN LABEL",
      descripcion: "Johnnie Walker Green Label is crafted from a palette of Speyside, Highland, Lowland and Island malts matured for at least 15 years.",
      imagen: "https://nocturnusdrink.com/12-large_default/whisky-johnnie-walker-green-label-750.jpg",
    },
    {
      id: 4,
      tipo: "ron",
      nombre: "FLOR DE CANA 18 ANOS",
      descripcion: "Un ron ultra-premium de 18 años producido de manera sostenible (certificado Carbono Neutral y Fair Trade) y con cero contenido de azúcar. Con 5 generaciones de historia familiar, es añejado naturalmente sin ingredientes artificiales, destilado 100% con energía renovable y certificado KOSHER. Un ron de cuerpo entero con un largo y suave acabado que perdura en el paladar.",
      imagen: "https://tubodegaweb.com/wp-content/uploads/2020/02/flordeca%C3%B1a.jpg",
    },
  ]

  constructor() { }
  getAll(){
    return [...this.bebidas];
  }

  getBebida(id: number){
    return {
      ...this.bebidas.find( bebida => {
        return id === bebida.id;
      })
    };
  }

  getTipo(type: string){
    var arreglo = new Array();

    for (var i = 0; i < this.bebidas.length; i++) {
      if (this.bebidas[i].tipo === type) {
        arreglo.push(this.bebidas[i]);
      }
    }
    return arreglo;
  }

  getCantidad(type: string){
    var cantidad = 0;

    for (var i = 0; i < this.bebidas.length; i++) {
      if (this.bebidas[i].tipo === type) {
        cantidad++;
      }
    }
    return cantidad;
  }

  getLastId(){
    var id = 0;

    for (var i = 0; i < this.bebidas.length; i++) {
    }

    id = this.bebidas[i-1].id;

    return id;
  }

  borrarLicor(id: number){
    for( var i = 0; i < this.bebidas.length; i++){ 
      if ( this.bebidas[i].id === id) { 
        this.bebidas.splice(i, 1); 
      }
    }
  }

  agregarLicor(name: string, desc: string, url: string, type: string, num: number){
    let trago: trago = {
      id: num,
      tipo: type,
      nombre: name,
      descripcion: desc,
      imagen: url
    };
    this.bebidas.push(trago);
  }
}
