import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'detalles',
    children:[
      {
        path: '',
        loadChildren: () => import('./detalles/detalles.module').then( m => m.DetallesPageModule)
      },
      {
        path: ':detalleID',
        loadChildren: () => import('./detalles/detalles.module').then( m => m.DetallesPageModule)
      }
    ]  
  },
  {
    path: 'licor',
    children:[
      {
        path: '',
        loadChildren: () => import('./licor/licor.module').then( m => m.LicorPageModule)
      },
      {
        path: ':tipoLicor',
        children:[
          {
            path: '',
            loadChildren: () => import('./licor/licor.module').then( m => m.LicorPageModule)
          },
          {
            path: ':detalleID',
            loadChildren: () => import('./detalles/detalles.module').then( m => m.DetallesPageModule)            
          }
        ]        
      }
    ] 
  },
  {
    path: 'agregar',
    children:[
      {
        path: '',
        loadChildren: () => import('./agregar/agregar.module').then( m => m.AgregarPageModule)
      },
      {
        path: ':tipoLicor',
        loadChildren: () => import('./agregar/agregar.module').then( m => m.AgregarPageModule)                
      }
    ]
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
