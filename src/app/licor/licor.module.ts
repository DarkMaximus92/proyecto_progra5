import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LicorPageRoutingModule } from './licor-routing.module';

import { LicorPage } from './licor.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LicorPageRoutingModule
  ],
  declarations: [LicorPage]
})
export class LicorPageModule {}
