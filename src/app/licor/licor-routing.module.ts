import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LicorPage } from './licor.page';

const routes: Routes = [
  {
    path: '',
    component: LicorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LicorPageRoutingModule {}
