import { Component, OnInit } from '@angular/core';
import { trago } from '../detalles/detalles.model';
import { ServiceService } from '../detalles/service.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-licor',
  templateUrl: './licor.page.html',
  styleUrls: ['./licor.page.scss'],
})
export class LicorPage implements OnInit {
  licores: trago [];
  tipoLicor: string;

  constructor(
    private route: ActivatedRoute,
    public servicio: ServiceService,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      if(!paramMap.has('tipoLicor')){
        return;
      }
      this.tipoLicor = paramMap.get('tipoLicor');
      this.licores = this.servicio.getTipo(this.tipoLicor);
    }); 
  }

  ionViewWillEnter(){
    this.route.paramMap.subscribe(paramMap => {
      if(!paramMap.has('tipoLicor')){
        return;
      }
      this.tipoLicor = paramMap.get('tipoLicor');
      this.licores = this.servicio.getTipo(this.tipoLicor);
    }); 
  }

  async filterList(evt) {
    const searchbar = document.querySelector('ion-searchbar');
    const items = Array.from(document.querySelector('ion-list').children);

    searchbar.addEventListener('ionInput', handleInput);

    function handleInput(event) {
      const query = event.target.value.toLowerCase();
      requestAnimationFrame(() => {
        items.forEach(item => {
          const shouldShow = item.textContent.toLowerCase().indexOf(query) > -1;
          item.style.display = shouldShow ? 'block' : 'none';
        });
      });
    }
  }
}
