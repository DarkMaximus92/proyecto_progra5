import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LicorPage } from './licor.page';

describe('LicorPage', () => {
  let component: LicorPage;
  let fixture: ComponentFixture<LicorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LicorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LicorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
