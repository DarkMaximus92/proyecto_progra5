import { Component, OnInit } from '@angular/core';
import { trago } from '../detalles/detalles.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceService } from '../detalles/service.service';
import { AlertController } from '@ionic/angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.page.html',
  styleUrls: ['./agregar.page.scss'],
})
export class AgregarPage implements OnInit {
  tipoLicor: string;
  lastID: number;
  form: FormGroup;

  constructor(
    private route: ActivatedRoute,
    public servicio: ServiceService,
    private router: Router,
    private alert: AlertController
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      if(!paramMap.has('tipoLicor')){
        return;
      }
      this.tipoLicor = paramMap.get('tipoLicor');
      this.lastID = this.servicio.getLastId();
    });

    this.form = new FormGroup({
      nombre: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required]
      }),
      descripcion: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required]
      }),
      url: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required]
      }),
    });
  }
  
  agregarLicor(){
    this.alert.create({
      header: 'Agregar',
      message: 'Esta seguro que quiere agregar este licor?',
      buttons:[
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'Agregar',
          handler: () => {
            this.servicio.agregarLicor(this.form.value.nombre, this.form.value.descripcion, this.form.value.url, this.tipoLicor, this.lastID+1);
            this.router.navigate(['/licor', this.tipoLicor]);
          }
        }
      ]
    })
    .then(alertEl => {
      alertEl.present();
    });
  }

}
